## v2.0.0 - 2023-07-19
### Added
* JR-99: Updated Changie config to add issue
### Changed
* JR-100: Changed the Changie noteline format
